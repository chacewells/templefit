from django.shortcuts import render, render_to_response
from programs.models import Program

def programs(request):
	fitness_programs = Program.objects.all()
	return render_to_response('programs.html', {'fitness_programs': fitness_programs})
