from django.db import models
from django.utils import timezone
import datetime

class Program(models.Model):
	prog_name = models.CharField(max_length=30)
	image = models.CharField(max_length=50)
	description = models.TextField(max_length=2000)

	def __unicode__(self):
		return self.prog_name

class TimeBlock(models.Model):
	MONDAY = 0
	TUESDAY = 1
	WEDNESDAY = 2
	THURSDAY = 3
	FRIDAY = 4
	SATURDAY = 5
	SUNDAY = 6

	DAY_OF_WEEK_CHOICES = (
		(MONDAY, 'Monday'),
		(TUESDAY, 'Tuesday'),
		(WEDNESDAY, 'Wednesday'),
		(THURSDAY, 'Thursday'),
		(FRIDAY, 'Friday'),
		(SATURDAY, 'Saturday'),
		(SUNDAY, 'Sunday'),
	)

	day_of_week = models.IntegerField(choices=DAY_OF_WEEK_CHOICES, default=MONDAY)
	time_of_day = models.IntegerField(default=datetime.timedelta(hours=8).seconds) # event time of day in seconds
	duration = models.IntegerField(default=datetime.timedelta(hours=1).seconds) # event duration in seconds
	program = models.ForeignKey(Program)

	def __unicode__(self):
		return "{}, {}, {} hour(s)".format(self.get_day_of_week(), self.get_time_of_day(), self.get_duration_hours())

	def get_day_of_week(self):
		return self.DAY_OF_WEEK_CHOICES[self.day_of_week][1]

	def time_of_day_timedelta(self):
		return datetime.timedelta(seconds=self.time_of_day)

	def get_time_of_day(self):
		hr = self.get_hours_minutes()[0]
		min = self.get_hours_minutes()[1]
		p = ""
		if hr < 11:
			p = "am"
			if hr == 0:
				hr = 12
		else:
			p = "pm"
		return "{}:{} {}".format(hr, str(min).zfill(2), p)
		
	def get_hours_minutes(self):
		td = self.time_of_day_timedelta()
		return td.seconds//3600, (td.seconds//60)%60

	def get_duration_hours(self):
		return self.duration//3600

	def get_duration_timedelta(self):
		return datetime.timedelta(seconds=self.duration)
