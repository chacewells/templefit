from django.shortcuts import render, render_to_response
from programs.models import Program
from testimonials.models import Testimonial
from mission_stmt.models import MissionStatement

def main(request):
	fitness_programs = Program.objects.all()
	testimonials = Testimonial.objects.all()
	mission_statement = MissionStatement.objects.get(pk=1)
	return render_to_response('main/index.html', {'fitness_programs': fitness_programs,
												  'testimonials': testimonials,
												  'mission_statement': mission_statement})
