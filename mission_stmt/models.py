from django.db import models

class MissionStatement(models.Model):
	title = models.CharField(max_length=50)
	content = models.TextField(max_length=3000)

	def __unicode__(self):
		return self.title
