from django.db import models

class Testimonial(models.Model):
	name = models.CharField(max_length=50)
	image = models.CharField(max_length=50)
	content = models.TextField(max_length=1000)

	def __unicode__(self):
		return self.name
